package com.xlogisticzz.learningModding.lib;

/**
 * Learning Modding Mod
 *
 * @author xLoGisTicZz. Some code may be from tutorials.
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 */

public class Ids {

    // Block IDs
    // Defaults
    public static int ORE_DEFAULT = 500;
    public static int STORAGE_BLOCK_DEFAULT = 501;
    public static int GLASS_CONNECTED_DEFAULT = 502;
    public static int TESTBLOCK_DEFAULT = 503;
    public static int MARKERS_DEFAULT = 504;
    public static int BOMB_DEFAULT = 505;
    public static int SUPERBOMB_DEFAULT = 506;
    public static int NUMBER_DEFAULT = 507;
    public static int CLICKER_DEFAULT = 508;
    public static int ENTITY_BLOCK_TELEPORTER_DEFAULT = 509;
    public static int POISON_DEFAULT = 510;
    public static int HEIGHTPARTICLE_DEFAULT = 511;
    public static int NOTE_SEQUENCER_DEFAULT = 512;
    public static int CAKE_STORAGE_DEFAULT = 512;
    public static int MACHINEBLOCK_DEFAULT = 513;

    // Current
    public static int ORE;
    public static int STORAGE_BLOCK;
    public static int MACHINEBLOCK;
    public static int GLASS_CONNECTED;
    public static int TESTBLOCK;
    public static int MARKERS;
    public static int BOMB;
    public static int SUPERBOMB;
    public static int NUMBER;
    public static int CLICKER;
    public static int ENTITY_BLOCK_TELEPORTER;
    public static int POISON;
    public static int HEIGHTPARTICLE;
    public static int NOTE_SEQUENCER;
    public static int CAKE_STORAGE;


    // Item IDs
    // Defaults
    public static int ITEMS_DEFAULT = 5000;
    public static int RUBY_SWORD_DEFAULT = 5001;
    public static int RUBY_PICKAXE_DEFAULT = 5002;
    public static int RUBY_SPADE_DEFAULT = 5003;
    public static int RUBY_AXE_DEFAULT = 5004;
    public static int RUBY_HOE_DEFAULT = 5005;
    public static int RUBY_HELMET_DEFAULT = 5006;
    public static int RUBY_CHESTPLATE_DEFAULT = 5007;
    public static int RUBY_LEGGINGS_DEFAULT = 5008;
    public static int RUBY_BOOTS_DEFAULT = 5009;
    public static int RUBY_WAND_DEFAULT = 5010;
    public static int CARD_DEFAULT = 5011;
    public static int DEATHSTONES_DEFAULT = 5012;
    public static int WAND_DEFAULT = 5013;
    public static int SPAWN_SPACESHIP_DEFAULT = 5014;
    public static int ENTITY_LAUNCHER_DEFAULT = 5015;
    public static int PIG_CONVERTER_DEFAULT = 5016;

    // Current
    public static int ITEMS;
    public static int RUBY_SWORD;
    public static int RUBY_PICKAXE;
    public static int RUBY_SPADE;
    public static int RUBY_AXE;
    public static int RUBY_HOE;
    public static int RUBY_HELMET;
    public static int RUBY_CHESTPLATE;
    public static int RUBY_LEGGINGS;
    public static int RUBY_BOOTS;
    public static int RUBY_WAND;
    public static int CARD;
    public static int DEATHSTONES;
    public static int WAND;
    public static int SPAWN_SPACESHIP;
    public static int ENTITY_LAUNCHER;
    public static int PIG_CONVERTER;

}
